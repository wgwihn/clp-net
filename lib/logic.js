/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
/**
 * Write your transction processor functions here
 */

/**
 * Transaction for finding customer by email
 * @param {org.example.clpnet.FindCustomerByEmail} findCustomerByEmail
 * @transaction
 */
async function findCustomerByEmail(tx) {

    const email = tx.email;
    const customers = await query('selectCustomerByEmail', {"email": email});

    let size = customers.length;
    if (size == 0) {
        return [];
    } else {
        return customers[0];
    }
}

/**
 * Transaction for finding all Business Unit
 * @param {org.example.clpnet.FindAllBusinessUnit} findAllBusinessUnit
 * @returns {org.example.clpnet.BusinessUnit[]} All the bu.
 * @transaction
 */
async function findAllBusinessUnit(tx) {

    const buRegistry = await getParticipantRegistry('org.example.clpnet.BusinessUnit');
    const localBUs = await buRegistry.getAll();

    const bus = [];
    if (localBUs.length > 0) {
        for (const bu of localBUs) {
            bus.push(bu);
        }
    }
    return bus;
}

/**
 * Transaction for customer to join a BU
 * @param {org.example.clpnet.SubmitJoinBu} submitJoinBu
 * @returns {org.example.clpnet.CustomerBuSheet} the new CustomerBuSheet
 * @transaction
 */
async function submitJoinBu(tx) {
    const cId = tx.cId;
    const buId = tx.buId;

    const factory = getFactory();

    const customerRegistry = await getParticipantRegistry('org.example.clpnet.Customer');
    const isCustomerExisted = await customerRegistry.exists(cId);

    const buRegistry = await getParticipantRegistry('org.example.clpnet.BusinessUnit');
    const isBUExisted = await buRegistry.exists(buId);

    if (!isCustomerExisted) {
        throw 'Customer does not exist';
    }

    if (!isBUExisted) {
        throw 'Bu does not exist';
    }

    const sheetId = 'Sx' + cId + '@' + buId;

    const sheetRegistry = await getAssetRegistry('org.example.clpnet.CustomerBuSheet');
    const isSheetExisted = await sheetRegistry.exists(sheetId);

    var sheet;
    if (isSheetExisted) {
        // TODO may activate the sheet
        sheet = await sheetRegistry.get(sheetId);
    } else {
        sheet = await factory.newResource('org.example.clpnet', 'CustomerBuSheet', sheetId);
        sheet.customer = factory.newRelationship('org.example.clpnet', 'Customer', cId);
        sheet.bu = factory.newRelationship('org.example.clpnet', 'BusinessUnit', buId);
        sheet.status = "Active";
        sheet.status = "Active";
        sheet.point = 0;
        sheet.joinDateTime = new Date();
        await sheetRegistry.add(sheet);
    }

    return sheet;

}

/**
 * Transaction for a new trade submission
 * @param {org.example.clpnet.SubmitNewTrade} submitNewTrade
 * @returns {org.example.clpnet.Trade} the new Trade
 * @transaction
 */
async function submitNewTrade(tx) {

    const tradeId = tx.tradeId;
    const cId = tx.customerId;
    const buId = tx.buId;
    const point = tx.point;
    const clientSubmitTime = tx.clientSubmitTime;
    const status = tx.status;

    const custReg = await getParticipantRegistry('org.example.clpnet.Customer');
    const customer = await custReg.get(cId);

    const buReg = await getParticipantRegistry('org.example.clpnet.BusinessUnit');
    const bu = await buReg.get(buId);

    if (!customer) {
        throw 'Customer does not exist';
    }

    if (!bu) {
        throw 'Bu does not exist';
    }

    try {
        const factory = getFactory();

        const sheet = await query('selectCustomerBuSheet',
            {"customer": 'resource:' + customer.getFullyQualifiedIdentifier(), "bu": 'resource:' + bu.getFullyQualifiedIdentifier()});

        if (!sheet || sheet.length < 1) {
            throw 'Customer does not join this shop.'
        }

        const newTrade = factory.newResource('org.example.clpnet', 'Trade', tradeId);
        newTrade.point = point;
        newTrade.customer = factory.newRelationship('org.example.clpnet', 'Customer', customer.getIdentifier());
        newTrade.bu = factory.newRelationship('org.example.clpnet', 'BusinessUnit', bu.getIdentifier());
        newTrade.clientSubmitTime = clientSubmitTime;
        newTrade.processTime = new Date();
        newTrade.status = status;

        sheet[0].point += point;
        const sheetRegistry = await getAssetRegistry('org.example.clpnet.CustomerBuSheet');
        await sheetRegistry.update(sheet[0]);

        const tradeRegistry = await getAssetRegistry('org.example.clpnet.Trade');
        await tradeRegistry.add(newTrade);

        return newTrade;
    } catch (err) {
        throw err;
    }
}

/**
 * Transaction to register BU
 * @param {org.example.clpnet.RegisterBusinessUnit} registerBusinessUnit
 * @transaction
 */
async function registerBusinessUnit(tx) {

    const buId = tx.buId;
    const name = tx.name;
    const ptToCoinRatio = tx.ptToCoinRatio;
    const coinToPtRatio = tx.coinToPtRatio;

    // TODO check bu id format 'Bx###'
    const buRegistry = await getParticipantRegistry('org.example.clpnet.BusinessUnit');
    const isBUExisted = await buRegistry.exists(buId);
    if (isBUExisted) {
        throw 'Bu Id is duplicated!';
    }

    const factory = getFactory();
    const bu = factory.newResource('org.example.clpnet', 'BusinessUnit', buId);
    bu.name = name;
    bu.ptToCoinRatio = ptToCoinRatio;
    bu.coinToPtRatio = coinToPtRatio;

    await buRegistry.add(bu);

    return bu;
}

/**
 * Transaction to find All Trades for particular customer and bu
 * @param {org.example.clpnet.FindBuCustomerTrades} findBuCustomerTrades
 * @returns {org.example.clpnet.Trade[]} the Trades
 * @transaction
 */
async function findBuCustomerTrades(tx) {

    const customerId = tx.customerId;
    const buId = tx.buId;

    const custReg = await getParticipantRegistry('org.example.clpnet.Customer');
    const customer = await custReg.get(cId);

    const buReg = await getParticipantRegistry('org.example.clpnet.BusinessUnit');
    const bu = await buReg.get(buId);

    if (!customer) {
        throw 'Customer does not exist';
    }

    if (!bu) {
        throw 'Bu does not exist';
    }

    const trades = await query('selectBuCustomerTrades', { "customer": customerId, "bu": buId });
    const result = [];

    let size = trades.length;
    if (size == 0) {
        return result;
    }

    for (const t of trades) {
        result.push(t);
    }
    return result;
}

/**
 * Transaction to exchange coin
 * @param {org.example.clpnet.SubmitExchangeCoin} submitExchangeCoin
 * @returns {org.example.clpnet.Trade} the new Trade
 * @transaction
 */
async function submitExchangeCoin(tx) {

    var error = [];

    const cId = tx.customerId;
    const tId = tx.tradeId;

    const custReg = await getParticipantRegistry('org.example.clpnet.Customer');
    const customer = await custReg.get(cId);
    if (!customer) {
        throw 'Customer does not exist';
    }

    const tradeReg = await getAssetRegistry('org.example.clpnet.Trade');
    const trade = await tradeReg.get(tId);

    if (!trade) {
        error.push('Trade does not exit.');
        throw error;
    }

    if (trade.status != "Active"/* || !(trade.usedTime) ||  === undefined*/) {
        error.push(`Trade is not Active or has already used.`);
        throw error;
    }

    const buReg = await getParticipantRegistry('org.example.clpnet.BusinessUnit');
    const bu = await buReg.get(trade.bu.getIdentifier());

    if (!bu) {
        error.push('Cannot find BU for this trade.' + JSON.stringify(trade.bu));
        throw error;
    }

    const sheet = await query('selectCustomerBuSheet',
        {"customer": 'resource:' + customer.getFullyQualifiedIdentifier(), "bu": 'resource:' + bu.getFullyQualifiedIdentifier()});

    if (!sheet || sheet.length < 1) {
        throw 'Customer does not join this shop.'
    }

    // calculate the coin
    const point = trade.point;
    const ptPerCoinRatio = bu.ptToCoinRatio;
    const coin = Math.round(point / ptPerCoinRatio * 100) / 100;

    // Reduce point
    sheet[0].point -= point;
    const sheetRegistry = await getAssetRegistry('org.example.clpnet.CustomerBuSheet');
    await sheetRegistry.update(sheet[0]);

    // Gain coin
    const coinRegistry = await getAssetRegistry('org.example.clpnet.Coin');
    const c = await coinRegistry.get(customer.customerId);

    c.value += coin;
    await coinRegistry.update(c);

    // Update trade staus
    trade.status = "Inactive";
    trade.usedTime = new Date();
    await tradeReg.update(trade);

    return trade;

}

/**
 * Transaction to create a new Reward Post
 * @param {org.example.clpnet.SubmitNewRewardPost} submitNewRewardPost
 * @returns {org.example.clpnet.RewardPost} success or failure
 * @transaction
 */
async function submitNewRewardPost(tx) {

    const rewardPostId = tx.rewardPostId;
    const issueBuId = tx.buId;
    const issueDate = new Date();
    const name = tx.name;
    const description = tx.description;
    const regulation = tx.regulation;
    const coin = tx.coin;
    const redemptionStartDate = tx.redemptionStartDate;
    const redemptionEndDate = tx.redemptionEndDate;
    const count = tx.count;
    const unlimited = tx.unlimited;

    // check rewardPostId is valid
    var rex = new RegExp("(RPx)[0-9]{5}(@Bx)[0-9]{3}$");
    if (!rex.test(rewardPostId)) {
        throw 'Reward post format is invalid.';
    }

    try {
        const factory = getFactory();

        const buReg = await getParticipantRegistry('org.example.clpnet.BusinessUnit');
        const issueBu = await buReg.get(issueBuId);

        const rewPostRegistry = await getAssetRegistry('org.example.clpnet.RewardPost');
        const isRewardPostIdExisted = await rewPostRegistry.exists(rewardPostId);
        if (isRewardPostIdExisted) {
            throw 'Reward Post Id is duplicated!';
        }

        // TODO check from/to date is valid

        const newRewardPost = factory.newResource('org.example.clpnet', 'RewardPost', rewardPostId);
        newRewardPost.issueBu = factory.newRelationship('org.example.clpnet', 'BusinessUnit', issueBu.getIdentifier());
        newRewardPost.issueDate = issueDate;
        newRewardPost.name  = name;
        newRewardPost.description = description;
        newRewardPost.regulation = regulation;
        newRewardPost.coin = coin;
        newRewardPost.redemptionStartDate = redemptionStartDate;
        newRewardPost.redemptionEndDate = redemptionEndDate;
        newRewardPost.count = count;
        newRewardPost.unlimited = unlimited;

        await rewPostRegistry.add(newRewardPost);

        return newRewardPost;
    } catch (err) {
        throw err;
    }
}

/**
 * Transaction to exchange a reward
 * @param {org.example.clpnet.SubmitExchangeReward} submitExchangeReward
 * @returns {org.example.clpnet.Reward} success or failure
 * @transaction
 */
async function submitExchangeReward(tx) {

    const rewardId = tx.rewardId;
    const cId = tx.customerId;
    const rewardPostId = tx.rewardPostId;
    const redeemTime = new Date();
    const status = "Active";

    // check rewardId is valid
    const rex = new RegExp("(Rx).+(@Cx).+$");
    if (!rex.test(rewardId)) {
        return false;
    }

    try {
        const factory = getFactory();

        const custReg = await getParticipantRegistry('org.example.clpnet.Customer');
        const customer = await custReg.get(cId);

        if (!customer) {
            throw 'Customer does not existed.';
        }

        const coinRegistry = await getAssetRegistry('org.example.clpnet.Coin');
        const coin = await coinRegistry.get(customer.customerId);

        if (!coin) {
            throw 'Coin does not existed.';
        }

        const rpReg = await getAssetRegistry('org.example.clpnet.RewardPost');
        const rewardPost = await rpReg.get(rewardPostId);

        if (!rewardPost) {
            throw 'Reward Post does not existed.';
        }

        if (redeemTime < rewardPost.redemptionStartDate || redeemTime > rewardPost.redemptionEndDate) {
            throw 'Not during effective redemption date. Exhange does not accept.';
        }

        if (!rewardPost.unlimited && rewardPost.count < 1) {
            throw 'Sorry that Reward is over exhanged.';
        }

        if (coin.value < rewardPost.coin) {
            throw 'Sorry that does not have enough Coin to exchange this reward.';
        }

        const newReward = factory.newResource('org.example.clpnet', 'Reward', rewardId);
        newReward.customer = factory.newRelationship('org.example.clpnet', 'Customer', customer.getIdentifier());
        newReward.referPost = factory.newRelationship('org.example.clpnet', 'RewardPost', rewardPost.getIdentifier());
        newReward.redeemTime = redeemTime;
        newReward.status = status;

        const rewRegistry = await getAssetRegistry('org.example.clpnet.Reward');
        await rewRegistry.add(newReward);

        rewardPost.count--;
        await rpReg.update(rewardPost);

        coin.value -= rewardPost.coin;
        await coinRegistry.update(coin);

        return newReward;
    } catch (err) {
        throw err;
    }
}

/**
 * Transaction to find All Trades for bu with particular period
 * @param {org.example.clpnet.FindAllTradesByBuId} findBuCustomerTrades
 * @returns {org.example.clpnet.Trade[]} the findAllTradesByBuId
 * @transaction
 */
async function findAllTradesByBuId(tx) {

    const buId = tx.buId;
    const from = tx.fromDateTime;
    const to = tx.toDateTime;

    // try {
    const trades = await query('selectTradesByBuInPeriod', { "bu": buId, "from": from, "to": to });
    const result = [];

    let size = trades.length;
    if (size == 0) {
        return result;
    }

    for (const t of trades) {
        result.push(t);
    }
    return result;
}


























